import './style.scss';
import $ from 'jquery';
import 'slick-carousel';

window.onload = function(){

  if(typeof window.orientation !== undefined){
    document.getElementById('fs-bg').className = "section first-screen mobile-bg";
  } else{
    document.getElementById('fs-bg').className = "section first-screen big-bg";
  }

  $('.carousel').slick({
    speed: 300,
    slidesToShow: 2,
    autoplay: true,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  var junior = document.getElementById('cat_junior');
  var catA = document.getElementById('cat_catA');
  var catB = document.getElementById('cat_catB');
  var progr = document.getElementById('dir_pro');
  var market = document.getElementById('dir_mar');
  var web = document.getElementById('dir_web');

  junior.checked = true;
  market.disabled = true;
  market.parentNode.className = "disabled";
  progr.checked = true;

  junior.onclick = function(event){
    market.disabled = true;
    web.disabled = false;
    market.parentNode.className = "disabled";
    web.parentNode.className = "";
    if (market.checked){
      progr.checked = true;
    }
  }

  catA.onclick = function(event){
    web.disabled = true;
    web.parentNode.className = "disabled";
    market.disabled = true;
    market.parentNode.className = "disabled";
    if (market.checked){
      progr.checked = true;
    }
    if (web.checked){
      progr.checked = true;
    }
  }

  catB.onclick = function(event){
    market.disabled = false;
    web.disabled = false;
    market.parentNode.className = "";
    web.parentNode.className = "";
  }
}
