var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Mailgun     = require('mailgun-js');
var ploy        = require('./ploy-client');

var routes = require('./routes');

var api         = ploy({
  key:    '23b3f3d04336fee75bd8f85fc4a43d85',
  secret: 'd9638038fe2952118e18b82fc0766e28'
});

var mailgun     = Mailgun({
  apiKey: 'key-25eb827f56213d9eff8a5df9053e407c',
  domain: 'sandboxaabd7df85cab447eab4c8e2983bcebaf.mailgun.org'
});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.post('/request',
  bodyParser.urlencoded({
    limit: '30mb',
    extended: false
  }),
  function(req, res) {

    if (req.body['category']){

      var parts = [
        'name',
        'birthdate',
        'school',
        'category',
        'direction',
        'phone',
        'email',
        'addition'
      ];

      var translates = {
        'name': 'Ф.И.О.',
        'birthdate': 'Дата рождения',
        'school': 'Учебное заведение',
        'category': 'Категория',
        'direction': 'Направление',
        'phone': 'Контактный номер',
        'email': 'Email',
        'addition': 'Дополнительная информация о себе'
      };
      
      var data = {
        from: 'no-reply <info@mp-it.ru>',
        to: req.body['email'],
        subject: 'Ссылка на конкурс "Моя профессия IT"',
        text: 'За один день до начала заочного отбора, мы вам отправим ссылку на тестирование. Спасибо за вашу заявку, удачи!'
      };

      var order_data = {
        from: 'no-reply <info@mp-it.ru>',
        to: 'info@mp-it.ru',
        subject: 'Заявка от участника',
        text: ''
      };      

      parts.forEach(function(part) {
        order_data.text+= translates[part] + ': ' + req.body[part]+'\n';
      });

      res.redirect('/request');

    } else{

      var parts = [
        'name',
        'phone',
        'email'
      ];

      var translates = {
        'name': 'Ф.И.О.',
        'phone': 'Контактный номер',
        'email': 'Email'
      };
      
      var data = {
        from: 'no-reply <info@mp-it.ru>',
        to: 'info@mp-it.ru',
        subject: 'Заявка на спонсорство',
        text: ''
      };

      parts.forEach(function(part) {
        data.text+= translates[part] + ': ' + req.body[part]+'\n';
      });

      res.redirect('/request');
    };
    
  });

app.use('/request', routes.request);
app.use('/', routes.index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
