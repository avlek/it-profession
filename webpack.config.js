var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: ['bootstrap-sass!./bootstrap-sass.config.js', './src/script.js'],

  output: {
    filename: 'build.js',
    path: path.resolve(__dirname, 'public/javascripts')
  },

  resolve: {
    extensions: ['', 'js', 'json', 'scss']
  },
  
  watch: true,

  module: {
    loaders: [{
      include: path.resolve(__dirname, 'src'),
      test: /\.js$/,
      loader: 'babel?presets[]=es2015'
    },{
      include: path.resolve(__dirname, 'src'),
      test: /\.scss/,
      loader: 'style!css!postcss!sass'
    }]
  },

  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
          warnings: false
      }
    })
  ],

  postcss: function () {
    return [require('postcss-clearfix')];
  }  
};