var path = require('path');

module.exports = {
  mainSass: path.resolve(__dirname, 'src/bootstrap.scss'),
  
  scripts: {
    // add every bootstrap script you need
    'transition': false
  },
  styles: {
    // add every bootstrap style you need
    "mixins": true,
    "normalize": true,
    "grid": true,
    "forms": true,
    "utilities": true,
    "responsive-utilities": true
  }
};